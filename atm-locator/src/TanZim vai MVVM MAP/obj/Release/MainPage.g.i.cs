﻿#pragma checksum "C:\Users\tareq\Documents\Visual Studio 2013\Projects\TanZim vai MVVM MAP\TanZim vai MVVM MAP\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D679916B68CCD8E07D33353E08A453BB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace TanZim_vai_MVVM_MAP {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.StackPanel _autoComplete;
        
        internal Microsoft.Phone.Controls.AutoCompleteBox _autoBankCompleteBox;
        
        internal Microsoft.Phone.Controls.AutoCompleteBox _autoAreaCompleteBox;
        
        internal System.Windows.Controls.Button _search;
        
        internal System.Windows.Controls.StackPanel _radiusSearch;
        
        internal Microsoft.Phone.Controls.AutoCompleteBox _autoNearCompleteBox;
        
        internal System.Windows.Controls.TextBox _radius;
        
        internal System.Windows.Controls.Button _radiusLoad;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal Microsoft.Phone.Maps.Controls.Map maps;
        
        internal System.Windows.Controls.Slider PitchSlider;
        
        internal System.Windows.Controls.Slider HeadingSlider;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/TanZim%20vai%20MVVM%20MAP;component/MainPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this._autoComplete = ((System.Windows.Controls.StackPanel)(this.FindName("_autoComplete")));
            this._autoBankCompleteBox = ((Microsoft.Phone.Controls.AutoCompleteBox)(this.FindName("_autoBankCompleteBox")));
            this._autoAreaCompleteBox = ((Microsoft.Phone.Controls.AutoCompleteBox)(this.FindName("_autoAreaCompleteBox")));
            this._search = ((System.Windows.Controls.Button)(this.FindName("_search")));
            this._radiusSearch = ((System.Windows.Controls.StackPanel)(this.FindName("_radiusSearch")));
            this._autoNearCompleteBox = ((Microsoft.Phone.Controls.AutoCompleteBox)(this.FindName("_autoNearCompleteBox")));
            this._radius = ((System.Windows.Controls.TextBox)(this.FindName("_radius")));
            this._radiusLoad = ((System.Windows.Controls.Button)(this.FindName("_radiusLoad")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.maps = ((Microsoft.Phone.Maps.Controls.Map)(this.FindName("maps")));
            this.PitchSlider = ((System.Windows.Controls.Slider)(this.FindName("PitchSlider")));
            this.HeadingSlider = ((System.Windows.Controls.Slider)(this.FindName("HeadingSlider")));
        }
    }
}

