﻿using GalaSoft.MvvmLight;
 
using Microsoft.Phone.Maps.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Device.Location;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TanZim_vai_MVVM_MAP
{
    public class Placemark : INotifyPropertyChanged
    {
        private string _Name;
        private string _Description;
        private GeoCoordinate _GeoCoordinate;
        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        public string Description
        {
            get
            {
                return this._Description;
            }

            set
            {
                if (this._Description != value)
                {
                    this._Description = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        [TypeConverter(typeof(GeoCoordinateConverter))]
        public GeoCoordinate GeoCoordinate
        {
            get
            {
                return this._GeoCoordinate;
            }

            set
            {
                if (this._GeoCoordinate != value)
                {
                    this._GeoCoordinate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    public class AtmBooth  

    {
        public string area { get; set; }
        
        public string postal_code { get; set; }
        public string district { get; set; }
        public string formatted_address { get; set; }
        public string formatted_phone_number { get; set; }
        public string international_phone_number { get; set; }
        public GeoCoordinate GeoCoordinate { get; set; }
        public string name { get; set; }
        public string shortname { get; set; }
        public OpeningHours opening_hours { get; set; }
        public string website { get; set; }

         
    }
    //public class GeoCoordinate
    //{
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }
    //}
    public class Close
    {
        public int day { get; set; }
        public string time { get; set; }
    }

    public class Open
    {
        public int day { get; set; }
        public string time { get; set; }
    }

    public class Period
    {
        public Close close { get; set; }
        public Open open { get; set; }
    }

    public class OpeningHours
    {
        public bool open_now { get; set; }
        public List<Period> periods { get; set; }
    }
    public class Nearest
    {
        public GeoCoordinate GeoCordinate { get; set; }
        public double distance { get; set; }
    }
}
