﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TanZim_vai_MVVM_MAP
{
    internal class PlacemarkViewModel
    {
        public PlacemarkViewModel()
        {
            this.Placemarks = new Placemarks();
        }

        /// <summary>
        /// Gets or sets the list of stores
        /// </summary>
        public Placemarks Placemarks { get; set; }
    }
}
