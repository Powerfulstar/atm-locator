using GalaSoft.MvvmLight;
using Microsoft.Phone.Maps.Controls;
using System.Device.Location; // Provides the GeoCoordinate class.
using Windows.Devices.Geolocation; //Provides the Geocoordinate class.
using System.Windows.Media;
using System.Windows.Shapes;
using TanZim_vai_MVVM_MAP;
using System;
using ShowMyLocationOnMap;
using GalaSoft.MvvmLight.Command;
namespace TanZim_vai_MVVM_MAP.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase 
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        /// 

        
        /// <summary>
        /// Gets or sets the list of stores
        /// </summary>
        public Placemarks Placemarks { get; set; }
        public GeoCoordinate myGeoCoordinate { get; set; }
        public RelayCommand getLocation { get; private set; }
        public bool isLocationEnabled = false;
        public MainViewModel()
        {
            this.Placemarks = new Placemarks();
            getLocation = new RelayCommand(() => ShowMyLocationOnTheMap());
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }
        
        private async void ShowMyLocationOnTheMap()
        {
            // Get my current location.
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            myGeoCoordinate = CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);

            //this.mapWithMyLocation.Center = myGeoCoordinate;
            //this.mapWithMyLocation.ZoomLevel = 13;
        }
 
             
      
    }
}