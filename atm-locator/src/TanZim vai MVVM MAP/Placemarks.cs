﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;


namespace TanZim_vai_MVVM_MAP
{
    public class Placemarks : ObservableCollection<AtmBooth>
    {
        public Placemarks()
        {
            this.LoadData();
        }
        
        
        private void LoadData()
        {
            StreamReader myFile = new StreamReader(@"Assets\atmboothlist.txt");
            string myString = myFile.ReadToEnd();
            ObservableCollection<AtmBooth> ob1 = new ObservableCollection<AtmBooth>();
            ob1 = JsonConvert.DeserializeObject<ObservableCollection<AtmBooth>>(myString);
            foreach (var item in ob1)
            {
                this.Add(item);
            }
           
            
        }
    }
}
